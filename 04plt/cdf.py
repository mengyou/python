
import matplotlib.pyplot as plt 

# 导入字体管理模块
# from matplotlib import font_manager
# my_font = font_manager.FontProperties(fname="C:/Windows/Fonts/msyh.ttc")


# 绘制CDF曲线
def cdf_of_var(var_name: str, data: list):
    # No of Data points
    plt.figure()
    # initializing random values
    # data = np.random.randn(N)

    # getting data of the histogram
    count, bins_count = np.histogram(data, bins=10)

    # finding the PDF of the histogram using count values
    pdf = count / sum(count)

    # using numpy np.cumsum to calculate the CDF
    # We can also find using the PDF values by looping and adding
    cdf = np.cumsum(pdf)

    # plotting PDF and CDF
    # plt.plot(bins_count[1:], pdf, color="red", label="PDF")
    plt.plot(bins_count[1:], cdf, label="CDF")
    # plt.xlabel(var_name, fontproperties=my_font)
    plt.xlabel(var_name)
    plt.legend()