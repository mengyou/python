
import matplotlib.pyplot as plt



fig = plt.figure(figsize=(8, 8)) 


for k in range(72):
    # 筛选出 orbit 列值等于 k 的数据
    orbit_df = filter_satellite_df[filter_satellite_df['orbit'] == k]
    # 按照轨道内相位排序 即 phase 排序
    orbit_df = orbit_df.sort_values(by='phase')
    new_phase_list = orbit_df['phase']

    plt.scatter(new_phase_list, [k]*len(new_phase_list), label=f"orbit {k}", color='black')


# 绘制 ring0_arr 环 当中 的卫星分布
ring_df = filter_satellite_df[filter_satellite_df['ring']==0]
# 按照轨道内相位排序 即 phase 排序
ring_df = ring_df.sort_values(by='phase')
plt.scatter(ring_df['phase'], ring_df['orbit'], color='red')

plt.title("Ring0 In Shell 2 Satellite Distribution")
plt.xlabel("Phase / Degree")
plt.ylabel("Orbit")
