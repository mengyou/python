

satno_dict = {}
for ephemeris_file in file_list:
    # print(ephemeris_file)
    key = extract_satno(ephemeris_file)
        
    if key not in satno_dict.keys():
        satno_dict[key] = [ephemeris_file]
    else:
        satno_dict[key].append(ephemeris_file)