

# 星历文件读取函数
import datetime
import re


def convert_doy(epoch):
    """
    转换日期为datetime类型
    输入是str
    """
    year = epoch[:4]
    day_num = epoch[4:7]
    t_clock = epoch[7:]
    day_num.rjust(3 + len(day_num), '0')
    t_clock = t_clock[:2] + ":" + t_clock[2:4] + ":" + t_clock[4:]
    res = datetime.datetime.strptime(year + "-" + day_num + " " + t_clock, "%Y-%j %H:%M:%S.%f")
    return res

def read_ephemeris(file_name):
    """
    读取 星历 数据
    """
    # 提取卫星编号名
    # rule = r'(\d+).txt'  # 正则规则
    rule = r'MEME_(\d+)'  # 正则规则
    
    sat_name = re.findall(rule, file_name)[0]

    with open(file_name, "r") as f:
        data = f.readlines()
        # 提取创建时间
        rule_start = r'created:(.*?) UTC'  # 正则规则
        created_time = re.findall(rule_start, data[0])[0]
        # 舍弃前4行数据
        data = data[4:]
        f.close()

    result = {'epoch': [], 'pos': [], 'velocity': [], 'altitude': [], 'radial velocity': []}

    # 按照步进值=4进行遍历
    for l in range(0, len(data), 4):
        t = data[l].split(" ")
        result['epoch'].append(convert_doy(t[0]))
        pos = [float(item) for item in t[1:4]]
        result['pos'].append(pos)
        vel = [float(item) for item in t[4:7]]
        result['velocity'].append(vel)
        alt = np.sqrt(pow(result['pos'][-1][0], 2) + pow(result['pos'][-1][1], 2) + pow(result['pos'][-1][2], 2))
        result['altitude'].append(alt)
        r_vel = vel[0] * (pos[0] / alt) + vel[1] * (pos[1] / alt) + vel[2] * (pos[2] / alt)
        result['radial velocity'].append(r_vel)

    del data
    return result, sat_name, created_time


# main
e_data, satno_name, created_time = read_ephemeris(ref_orbit_file_name)    #name用于存储
print(f"ref satellite: {satno_name}")