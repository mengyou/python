# os文件路径处理函数包 ver2.0


import os

# 返回指定路径下包含关键字的所有的文件的绝对路径；不包含文件夹；深度仅为 1
def list_files(path, check_keyword=None):
    # 返回指定路径下的  文件 绝对路径列表，只有一层
    result = []
    for root, dirs, files in os.walk(path, topdown=True):
        for name in files:
            if not check_keyword is None and check_keyword not in name:
                continue
            if "checkpoint" not in name:
                result.append(os.path.join(root, name))

        # 只需要单层
        break
    return sorted(result)

# 返回指定路径下包含关键字的所有的文件夹的绝对路径；深度仅为 1
def list_dirs(path, check_keyword=None):
    # 读取指定路径下的全部文件夹，只有第一层
    result = []
    for root, dirs, files in os.walk(path, topdown=True):
        for name in dirs:
            if not check_keyword is None and check_keyword not in name:
                continue
            if "checkpoint" not in name:
                result.append(os.path.join(root, name))
        break
    return sorted(result)

# 创建一个新文件夹；如存在则不执行操作
def generate_dir(root_temp, sub_dir_temp=None):
    if sub_dir_temp is None:
        # 只有一个参数输入，那就自动自取最后一个为sub_dir
        root = root_temp[:root_temp.rfind('/')]
        sub_dir_name = root_temp[root_temp.rfind('/') + 1:]
    else:
        root = root_temp
        sub_dir_name = sub_dir_temp

    # 创建一个目录，如已经存在就不操作
    if not os.path.exists(root):
        raise "root path does not exist"

    ab_path = os.path.join(root, sub_dir_name)
    # print(ab_path, 'hello')
    if os.path.exists(ab_path):
        pass
    else:
        os.mkdir(ab_path)

    return ab_path