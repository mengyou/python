
import pandas as pd


# 根据已有的数据创建df
DATAFRAME = df.DataFrame(DATA_LIST, columns = ['satno', 'pos', 'altitude', 'OBJECT_NAME', 'OBJECT_TYPE'])


# 创建一个空df
DATAFRAME = pd.DataFrame(
    columns=[
        "satno",
        "raan",
        "phase",
        "angle_v",
        "virtual_phase",
        "orbit",
        "orbit_loc",
    ]
)

# 将某一列设置为索引，例如第一列
DATAFRAME.set_index('satno', inplace=True)


print(f"DATAFRAME.shape = {DATAFRAME.shape}")
print(DATAFRAME.head())