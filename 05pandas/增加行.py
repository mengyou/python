

patch_name = f"orbit{int(reserved_df['orbit'][0]):02}_loc_{i:02}"
reserved_df.loc[patch_name] = pd.Series(
    {
        "raan": 0,
        "key2": patch_name
    }
)